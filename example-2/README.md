# Example II: Integration into Multi-Module Maven Project

Now lets enhance our previous example, by using maven modules.

## Build

```sh
# build backend & frontend
mvn clean install

# run backend & frontend
cd backend && mvn spring-boot:run
cd frontend && ng serve
```

## Maven structure

First let's create our root `pom.xml`.
There we reference our child modules and keep version information.

<details>
<summary><strong>Expand: pom.yaml</strong></summary>

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>2.4.1</version>
		<relativePath/> <!-- lookup parent from repository -->
	</parent>

	<groupId>com.example</groupId>
	<artifactId>coffee</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<name>coffee</name>
	<description>Code Generation with Spring and Angular</description>

    <packaging>pom</packaging>

    <modules>
        <module>api</module>
        <module>backend</module>
        <module>frontend</module>
    </modules>

	<properties>
		<java.version>11</java.version>

        <!-- OpenAPI Dependencies -->
        <openapi-generator-maven-plugin-version>4.3.1</openapi-generator-maven-plugin-version>
        <springdoc-openapi-ui-version>1.5.2</springdoc-openapi-ui-version>
        <springfox-version>2.9.2</springfox-version>
        <jackson-databind-nullable-version>0.2.1</jackson-databind-nullable-version>

        <!-- Frontend Maven Plugin -->
        <frontend.maven.plugin.version>1.10.4</frontend.maven.plugin.version>
        <frontend.maven.plugin.node.version>v12.16.1</frontend.maven.plugin.node.version>
        <frontend.maven.plugin.npm.version>6.13.4</frontend.maven.plugin.npm.version>
	</properties>

</project>
```

</details>

## API

Our API module has a simple pom.xml.

<details>
<summary><strong>Expand: api/pom.yaml</strong></summary>

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<parent>
		<groupId>com.example</groupId>
		<artifactId>coffee</artifactId>
		<version>0.0.1-SNAPSHOT</version>
        <relativePath>../</relativePath>
	</parent>

    <artifactId>coffee-api</artifactId>

</project>
```

</details>

We move the `coffee.yaml` file into `api/src/main/resources/`, so that maven can package it.
And because our backend and frontend depend on our API module, we need to install it to our local maven repository with `mvn clean install`.
This enables us to start our backend/frontend.

## Backend

Since we keep track of our versions in the parent pom, we remove it from the backend.
We also specify the `coffee-api` jar as a dependency in the openapi-generator configuration.

<details>
<summary><strong>Expand: backend/pom.yaml</strong></summary>

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>com.example</groupId>
        <artifactId>coffee</artifactId>
        <version>0.0.1-SNAPSHOT</version>
        <relativePath>../</relativePath>
    </parent>

    <artifactId>coffee-backend</artifactId>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-devtools</artifactId>
            <scope>runtime</scope>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-configuration-processor</artifactId>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>

        <!-- OpenAPI dependencies -->
        <dependency>
            <groupId>org.springdoc</groupId>
            <artifactId>springdoc-openapi-ui</artifactId>
            <version>${springdoc-openapi-ui-version}</version>
        </dependency>
        <dependency>
            <groupId>io.springfox</groupId>
            <artifactId>springfox-swagger2</artifactId>
            <version>${springfox-version}</version>
        </dependency>
        <dependency>
            <groupId>org.openapitools</groupId>
            <artifactId>jackson-databind-nullable</artifactId>
            <version>${jackson-databind-nullable-version}</version>
        </dependency>
        <!-- Bean Validation API support -->
        <dependency>
            <groupId>javax.validation</groupId>
            <artifactId>validation-api</artifactId>
        </dependency>

    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>

            <!-- API Code-Generation -->
            <plugin>
                <groupId>org.openapitools</groupId>
                <artifactId>openapi-generator-maven-plugin</artifactId>
                <version>${openapi-generator-maven-plugin-version}</version>
                <dependencies>
                    <dependency>
                        <groupId>${project.groupId}</groupId>
                        <artifactId>coffee-api</artifactId>
                        <version>${project.parent.version}</version>
                    </dependency>
                </dependencies>
                <executions>
                    <execution>
                        <goals>
                            <goal>generate</goal>
                        </goals>
                        <configuration>
                            <inputSpec>coffee.yaml</inputSpec>
                            <generatorName>spring</generatorName>
                            <configurationFile>${project.basedir}/src/main/resources/config/openapi-generator-config.json</configurationFile>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

        </plugins>
    </build>

</project>
```

</details>

The `spring` generator configuration is the same as in Example I.

## Frontend

We configure `openapi-generator-maven-plugin` similar to the backend, except we add the `<output>` path.

<details>
<summary><strong>Expand: frontend/pom.yaml</strong></summary>

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>com.example</groupId>
        <artifactId>coffee</artifactId>
        <version>0.0.1-SNAPSHOT</version>
        <relativePath>../</relativePath>
    </parent>

    <artifactId>coffee-frontend</artifactId>

    <build>
        <plugins>

            <!-- API Code-Generation -->
            <plugin>
                <groupId>org.openapitools</groupId>
                <artifactId>openapi-generator-maven-plugin</artifactId>
                <version>${openapi-generator-maven-plugin-version}</version>
                <dependencies>
                    <dependency>
                        <groupId>${project.groupId}</groupId>
                        <artifactId>coffee-api</artifactId>
                        <version>${project.parent.version}</version>
                    </dependency>
                </dependencies>
                <executions>
                    <execution>
                        <goals>
                            <goal>generate</goal>
                        </goals>
                        <configuration>
                            <inputSpec>coffee.yaml</inputSpec>
                            <generatorName>typescript-angular</generatorName>
                            <configurationFile>${project.basedir}/openapi-generator-config.json</configurationFile>
                            <output>${project.basedir}/src/coffee-api</output>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

        </plugins>
    </build>

</project>
```

</details>

Here's the `openapi-generator-config.json` of the `typescript-generator` generator:

```json
{
  "supportsES6": "true",
  "modelNameSuffix": "DTO"
}
```

### frontend-maven-plugin

Whenever we build, we want to install our frontend dependencies.

<details>
<summary><strong>Expand: frontend/pom.yaml build section</strong></summary>

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    ...

    <build>
        <plugins>

            ...

            <plugin>
              <groupId>com.github.eirslett</groupId>
              <artifactId>frontend-maven-plugin</artifactId>
              <executions>

                <execution>
                  <id>install node and npm</id>
                  <goals>
                    <goal>install-node-and-npm</goal>
                  </goals>
                  <configuration>
                    <nodeVersion>${frontend.maven.plugin.node.version}</nodeVersion>
                    <npmVersion>${frontend.maven.plugin.npm.version}</npmVersion>
                  </configuration>
                </execution>

                <execution>
                  <id>npm ci</id>
                  <goals>
                    <goal>npm</goal>
                  </goals>
                  <configuration>
                    <arguments>ci</arguments>
                    <npmInheritsProxyConfigFromMaven>false</npmInheritsProxyConfigFromMaven>
                  </configuration>
                </execution>

                <execution>
                  <id>npm run-script-compile</id>
                  <phase>compile</phase>
                  <goals>
                    <goal>npm</goal>
                  </goals>
                  <configuration>
                    <arguments>run build</arguments>
                    <npmInheritsProxyConfigFromMaven>false</npmInheritsProxyConfigFromMaven>
                  </configuration>
                </execution>

              </executions>
            </plugin>

        </plugins>
    </build>

</project>
```

</details>
