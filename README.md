# Example OpenAPI Application using Spring and Angular

The repository shows you how to integrate **Code-Generation** into your **Spring/Angular** project.
An API-First approach is taken, where the API specification is the source of truth.
Code-generation is done using the [openapi-generator](https://github.com/OpenAPITools/openapi-generator) tools, specifically the [openapi-generator-cli](https://github.com/OpenAPITools/openapi-generator-cli) and the [openapi-generator-maven-plugin](https://github.com/OpenAPITools/openapi-generator/tree/master/modules/openapi-generator-maven-plugin).

Two examples are shown:

 * **[Example I](example-1/README.md):** Integration into standalone Spring/Angular Projects

   This example shows how to integrate the generated code into two standalone applications.

* **[Example II:](example-2/README.md)** Integration into Multi-Module Maven Project

    This example demonstrates a more involved setup, using multiple maven modules.
    It consists of the parent module and three child modules: api, backend and frontend.
    The [frontend-maven-plugin](https://github.com/eirslett/frontend-maven-plugin) is used to integrate the frontend build system into maven.

## Coffee Application

Our example coffee application consists of the following components:
* A **Backend** exposing a single Endpoint at `http://localhost:8080/api/v1/coffee`. It returns a random coffee as JSON.
* A **Frontend** with a single page at `http://localhost:4200/coffee`. It's only purpose is to display the received coffee from the backend.

We use this API as a starting Point. It is described using the [OpenAPI Specification](https://swagger.io/specification/).

<details>
<summary><strong>Expand: coffee.yaml</strong></summary>

```yaml
openapi: 3.0.3
info:
  title: Coffee API
  description: API description in Markdown.
  version: 1.0.0
servers:
  - url: 'http://localhost:8080/api/v1'
paths:
  /coffee:
    get:
      tags:
      - coffee
      operationId: getCoffee
      summary: Returns a random coffee.
      description: Some delicious coffee
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Coffee'
components:
  schemas:
    Coffee:
      description: A delicious coffee
      required:
        - id
        - name
      properties:
        id:
          readOnly: true
          type: string
          format: uuid
        name:
          type: string
          example: capuccino
```

</details>
