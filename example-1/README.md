# Example I: Integration into standalone Spring/Angular Projects

This example shows how to generate server (**Spring**) and client (**Angular/Typescript**) code from an OpenAPI-Specification.

## Build

```sh
# build & start backend
cd backend && mvn clean package spring-boot:run

# build & start frontend
cd frontend && npm run start
```

## Introduction

There are multiple ways on how to generate code from an API Specification:
 * [Online Generators](https://api.openapi-generator.tech)
 * [openapi-generator](https://github.com/OpenAPITools/openapi-generator)
 * [openapi-generator-cli](https://github.com/OpenAPITools/openapi-generator-cli)
 * [openapi-generator-maven-plugin](https://github.com/OpenAPITools/openapi-generator/tree/master/modules/openapi-generator-maven-plugin)
 * [openapi-generator-gradle-plugin](https://github.com/OpenAPITools/openapi-generator/tree/master/modules/openapi-generator-gradle-plugin)

For the backend, we use the `openapi-generator-maven-plugin` to generate a `CoffeeAPI` interface and a `CoffeeDTO` class.
We then implement a Controller, that implements that interface.

For the frontend, we use the `openapi-generator-cli` to generate a `coffee.service.ts` service that we use in our own `coffee.component.tst`.

## Generating Server/Client mock projects: `openapi-generator-cli`

It is a good idea to fine-tune the code-generation, before we integrate it into our existing project.
We do this by generating some throwaway code, and tweaking the configuration.

The CLI-Tool is available as an NPM-package. We can install it with:

    npm install @openapitools/openapi-generator-cli

Checkout the [documentation](https://github.com/OpenAPITools/openapi-generator/blob/master/docs/usage.md) or display the help:

    npx openapi-generator-cli help

You can validate our specification:

    npx openapi-generator-cli validate -i coffee.yaml

List [Available Generators](https://github.com/OpenAPITools/openapi-generator/blob/master/docs/generators/README.md):

    npx openapi-generator-cli list

List Generators Options (i.e
[spring](https://github.com/OpenAPITools/openapi-generator/blob/master/docs/generators/spring.md)
or [typescript-angular](https://github.com/OpenAPITools/openapi-generator/blob/master/docs/generators/typescript-angular.md))

    npx openapi-generator-cli config-help -g spring
    npx openapi-generator-cli config-help -g typescript-angular

Generate Server/Client Code:

    npx openapi-generator-cli generate -i coffee.yaml -o openapi-backend  -g spring
    npx openapi-generator-cli generate -i coffee.yaml -o openapi-frontend -g typescript-angular

By default, this will create a standalone spring and angular project.
If we want to modify the generated Code, we need to adjust the generator settings.

### Tweaking generator properties

When using the CLI, we can directly pass in generator properties using the `-p` argument.

    # generate backend
    npx openapi-generator-cli generate -i coffee.yaml -o openapi-backend \
            -g spring -p interfaceOnly=true,modelNameSuffix="DTO"

    # generate frontend
    npx openapi-generator-cli generate -i coffee.yaml -o openapi-frontend \
            -g typescript-angular -p npmName="@coffee-api/api"

## Backend

First we create a new spring project from https://start.spring.io/:

    mkdir backend && cd backend
    curl https://start.spring.io/starter.tgz \
             -d artifactId=coffee-backend \
             -d name=coffee-backend \
             -d 'description=Code Generation with Spring and Angular' \
             -d dependencies=devtools,configuration-processor,web |
             tar -xzvf -

Then we configure the `openapi-generator-maven-plugin` in the `pom.xml`.

We require additional dependencies to be able to build our backend.
We can lookup the required dependencies in the throwaway project `openapi-backend`.

<details>
<summary>Expand <strong>backend/pom.xml</strong></summary>

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.4.1</version>
        <relativePath/> <!-- lookup parent from repository -->
    </parent>

    <groupId>com.example</groupId>
    <artifactId>coffee-backend</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <name>coffee-backend</name>
    <description>Code Generation with Spring and Angular</description>

    <properties>
        <java.version>11</java.version>
        <openapi-generator-maven-plugin-version>4.3.1</openapi-generator-maven-plugin-version>
        <springdoc-openapi-ui-version>1.5.2</springdoc-openapi-ui-version>
        <springfox-version>2.9.2</springfox-version>
        <jackson-databind-nullable-version>0.2.1</jackson-databind-nullable-version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-devtools</artifactId>
            <scope>runtime</scope>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-configuration-processor</artifactId>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>

        <!-- OpenAPI dependencies -->
        <dependency>
            <groupId>org.springdoc</groupId>
            <artifactId>springdoc-openapi-ui</artifactId>
            <version>${springdoc-openapi-ui-version}</version>
        </dependency>
        <dependency>
            <groupId>io.springfox</groupId>
            <artifactId>springfox-swagger2</artifactId>
            <version>${springfox-version}</version>
        </dependency>
        <dependency>
            <groupId>org.openapitools</groupId>
            <artifactId>jackson-databind-nullable</artifactId>
            <version>${jackson-databind-nullable-version}</version>
        </dependency>
        <!-- Bean Validation API support -->
        <dependency>
            <groupId>javax.validation</groupId>
            <artifactId>validation-api</artifactId>
        </dependency>

    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>

            <!-- API Code-Generation -->
            <plugin>
                <groupId>org.openapitools</groupId>
                <artifactId>openapi-generator-maven-plugin</artifactId>
                <version>${openapi-generator-maven-plugin-version}</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>generate</goal>
                        </goals>
                        <configuration>
                            <generatorName>spring</generatorName>
                            <inputSpec>${project.basedir}/../api/coffee.yaml</inputSpec>
                            <configurationFile>${project.basedir}/src/main/resources/config/openapi-generator-config.json</configurationFile>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

        </plugins>
    </build>

</project>
```

</details>

Here's the `openapi-generator-config.json` of the `spring` generator:

```json
{
  "interfaceOnly": "true",
  "modelNameSuffix": "DTO",
  "packageName": "com.example.coffee.api",
  "apiPackage": "com.example.coffee.api",
  "modelPackage": "com.example.coffee.api"
}
```

### Swagger UI

The `springfox-swagger-ui` dependency from the `openapi-backend` project will setup the [swagger-ui](https://swagger.io/tools/swagger-ui/).
However, it requires some initial configuration.

Therefore I will use `springdoc-openapi-ui` package, that relies on auto-configuration.

Now the API-documentation is available at `http://localhost:8080/swagger-ui`.
We can also view the API documentation as JSON here `http://localhost:8080/v3/api-docs`, or download the API as YAML here `http://localhost:8080/v3/api-docs.yaml`.

### Creating a RestController

The only step left ist to actually use the generated classes.

Now let's create a controller, that extends the interface.
Additionally, we copy over the `@RequestMapping` from the `openapi-backend`.
We'll also add `@CrossOrigin`, to add the CORS header.

```java
@CrossOrigin
@RestController
@RequestMapping("${openapi.coffee.base-path:/api/v1}")
class CoffeeApiController implements CoffeeApi {
    @Override
    public ResponseEntity<CoffeeDTO> getCoffee() {
        CoffeeDTO coffee = new CoffeeDTO()
            .id(UUID.randomUUID())
            .name("capuccino");
        return ResponseEntity.of(Optional.of(coffee));
    }
}
```

## Frontend

Let's create a new frontend project with `ng new coffee-frontend`.
We enable `routing` and select `CSS` as the stylesheet format.

Now we add a simple component, that displays a coffee.

    ng generate component coffee

We modify the `frontend/src/app/coffee/coffee.component.ts` like so:

```typescript
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-coffee',
  templateUrl: './coffee.component.html',
  styleUrls: ['./coffee.component.css']
})
export class CoffeeComponent implements OnInit {
  coffee = { id: "1", name: "default" };
  constructor() { }
  ngOnInit(): void { }
}
```

And the `frontend/src/app/coffee/coffee.component.html` like so:

```typescript
<p>coffee works!</p>
<p>id: {{ coffee.id }}, name: {{ coffee.name }}</p>
```

### Default Route

To use this component as a starting point, we can add a default route to the `app-routing.module.ts`.

<details>
<summary>Expand <strong>frontend/src/app/app-routing.module.ts</strong></summary>

```typescript
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CoffeeComponent } from './coffee/coffee.component';

const routes: Routes = [
  { path: '', redirectTo: 'coffee', pathMatch: 'full' },
  { path: 'coffee', component: CoffeeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
```

We also clear the default angular template in `frontend/src/app/app.component.html`, and only leave the `<router-outlet>`.

```typescript
<h1>Example OpenAPI Application using Spring and Angular</h1>

<router-outlet></router-outlet>
```

</details>

### Frontend Code-Generation

For the code-generation, we install the `openapi-generator-cli`.

We can also configure the `typescript-angular` generator in the `openapitools.json` file.

```json
{
  "$schema": "node_modules/@openapitools/openapi-generator-cli/config.schema.json",
  "spaces": 2,
  "generator-cli": {
    "version": "5.0.0",
    "generators": {
      "coffee": {
        "generatorName": "typescript-angular",
        "output": "#{cwd}/src/#{name}-api",
        "glob": "../api/coffee.yaml",
        "additionalProperties": {
          "modelNameSuffix": "DTO",
          "supportsES6": "true"
        }
      }
    }
  }
}
```

Then we only need to issue the `npx openapi-generator-cli generate` command.

Now a angular module has been generated in `frontend/src/coffee-api`.
As specified in it's README.md, we add the `ApiModule` and the `HttpClientModule` to the `imports` section of our `frontend/src/app/app.module.ts`:

```typescript
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoffeeComponent } from './coffee/coffee.component';
import { HttpClientModule } from '@angular/common/http';
import { ApiModule } from 'src/coffee-api';

@NgModule({
  declarations: [
    AppComponent,
    CoffeeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ApiModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

Then we can use the generated `coffee.service.ts` in our coffee component:

```typescript
import { Component, OnInit } from '@angular/core';
import { CoffeeDTO, CoffeeService } from 'src/coffee-api';

@Component({
  selector: 'app-coffee',
  templateUrl: './coffee.component.html',
  styleUrls: ['./coffee.component.css']
})
export class CoffeeComponent implements OnInit {

  coffee : CoffeeDTO = { id: "1", name: "default" };

  constructor(private readonly coffeeService: CoffeeService) { }

  ngOnInit(): void {
    this.coffeeService.getCoffee().subscribe(coffee => {
      this.coffee = coffee;
    })
  }

}
```

To ensure the generation of the API module on startup, we can add a `prestart` script to our `frontend/package.json`.

```json
{
  "name": "coffee-frontend",
  "version": "0.0.0",
  "scripts": {
    "ng": "ng",
    "prestart": "npx openapi-generator-cli generate",
    "start": "ng serve",
    "build": "ng build",
    "test": "ng test",
    "lint": "ng lint",
    "e2e": "ng e2e"
  },
  ...
}
```
