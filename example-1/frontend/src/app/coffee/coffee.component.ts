import { Component, OnInit } from '@angular/core';
import { CoffeeDTO, CoffeeService } from 'src/coffee-api';

@Component({
  selector: 'app-coffee',
  templateUrl: './coffee.component.html',
  styleUrls: ['./coffee.component.css']
})
export class CoffeeComponent implements OnInit {

  coffee : CoffeeDTO = { id: "1", name: "default" };

  constructor(private readonly coffeeService: CoffeeService) { }

  ngOnInit(): void {
    this.coffeeService.getCoffee().subscribe(coffee => {
      this.coffee = coffee;
    })
  }

}
