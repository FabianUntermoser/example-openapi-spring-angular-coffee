package com.example.coffeebackend;

import java.util.Optional;
import java.util.UUID;

import com.example.coffee.api.CoffeeApi;
import com.example.coffee.api.CoffeeDTO;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class CoffeeBackendApplication {
    public static void main(String[] args) {
        SpringApplication.run(CoffeeBackendApplication.class, args);
    }
}

@CrossOrigin
@RestController
@RequestMapping("${openapi.coffee.base-path:/api/v1}")
class CoffeeApiController implements CoffeeApi {
    @Override
    public ResponseEntity<CoffeeDTO> getCoffee() {
        CoffeeDTO coffee = new CoffeeDTO()
            .id(UUID.randomUUID())
            .name("capuccino");
        return ResponseEntity.of(Optional.of(coffee));
    }
}
